//
//  JokeDetailViewModel.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation
import RxSwift

class JokeDetailViewModel {
    
    private var jokes = [Joke]()

    private var cellViewModels: [JokeDetailCellViewModel] = [JokeDetailCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    let disposeBag = DisposeBag()
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    func initFetch(category: String) {
        self.isLoading = false
        
        ChuckManager.getJoke(category: category).subscribe(onNext: { (result) in
            print(result)
            self.processFetchedJoke(joke: result)
        }, onError: { (error) in
            print("OPSSSS, temos um probleminha ==> \(error.localizedDescription)")
        }, onCompleted: {
            print("onCompleted")
            
        }).disposed(by: disposeBag)
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> JokeDetailCellViewModel {
           return cellViewModels[indexPath.row]
    }
    
    private func processFetchedJoke(joke: Joke) {
        self.jokes = [joke] // Cache
        var vms = [JokeDetailCellViewModel]()
        for joke in jokes {
            vms.append(createCellViewModel(imgUrl: joke.icon_url ?? "", joke: joke.value ?? ""))
        }
        self.cellViewModels = vms
    }
    
    func createCellViewModel(imgUrl: String, joke: String) -> JokeDetailCellViewModel {
        return JokeDetailCellViewModel(imgUrl: imgUrl, joke: joke)
    }
}

struct JokeDetailCellViewModel {
    let imgUrl: String
    let joke: String
}
