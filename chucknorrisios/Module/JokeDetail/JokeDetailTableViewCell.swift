//
//  JokeDetailTableViewCell.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 06/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import UIKit
import SDWebImage

class JokeDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var jokeLabel: UILabel!
    
    var jokeDetailCellViewModel : JokeDetailCellViewModel? {
          didSet {
            if let url = jokeDetailCellViewModel?.imgUrl {
                imageView?.sd_setImage(with: URL(string: url))
            }
            jokeLabel.text = jokeDetailCellViewModel?.joke
          }
      }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
