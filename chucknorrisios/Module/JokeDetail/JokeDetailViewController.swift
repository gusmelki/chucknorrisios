//
//  JokeDetailViewController.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import UIKit

class JokeDetailViewController: UIViewController {
    
    var category: String?
    @IBOutlet weak var tableView: UITableView!
    
    lazy var viewModel: JokeDetailViewModel = {
        return JokeDetailViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVM()
    }
    
    func initVM() {
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.initFetch(category: category ?? "")
    }
}

extension JokeDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "jokeCell", for: indexPath) as? JokeDetailTableViewCell else {
            fatalError("Cell not exists in storyboard")
        }
        
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.jokeDetailCellViewModel = cellVM
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    
}
