//
//  CategoryListViewModel.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation
import RxSwift


class CategoryListViewModel {
    
    let apiService: ChuckManagerProtocol
    
    private var categories = [String]()
    var selectedCategory: String?
    
    private var cellViewModels: [CategoryListCellViewModel] = [CategoryListCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    let disposeBag = DisposeBag()
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    init( apiService: ChuckManagerProtocol = ChuckManager()) {
        self.apiService = apiService
    }
    
    
    func initFetch() {
        self.isLoading = false
        
        ChuckManager.getCategories().subscribe(onNext: { (result) in
            print(result)
            self.processFetchedCategory(categories: result)
        }, onError: { (error) in
            print("OPSSSS, temos um probleminha ==> \(error.localizedDescription)")
        }, onCompleted: {
            print("onCompleted")
            
        }).disposed(by: disposeBag)
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> CategoryListCellViewModel {
           return cellViewModels[indexPath.row]
    }
    
    private func processFetchedCategory(categories: [String]) {
        self.categories = categories // Cache
        var vms = [CategoryListCellViewModel]()
        for category in categories {
            vms.append( createCellViewModel(category: category) )
        }
        self.cellViewModels = vms
    }
    
    func createCellViewModel(category: String) -> CategoryListCellViewModel {
        return CategoryListCellViewModel(category: category)
    }
    
    
}

extension CategoryListViewModel {
    func userPressed(at indexPath: IndexPath){
        let category = self.categories[indexPath.row]
        self.selectedCategory = category
    }
}

struct CategoryListCellViewModel {
    let category: String
}
