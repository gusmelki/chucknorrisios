//
//  CategoryListViewController.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import UIKit

class CategoryListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var viewModel: CategoryListViewModel = {
        return CategoryListViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initVM()
    }
    
    func initView() {
        self.navigationItem.title = "Categories"
        self.navigationController?.navigationBar.barTintColor = UIColor.systemYellow
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    func initVM() {
           
        //Naive binding
           viewModel.updateLoadingStatus = { [weak self] () in
               DispatchQueue.main.async {
                   let isLoading = self?.viewModel.isLoading ?? false
                   if isLoading {
                       self?.activityIndicator.startAnimating()
                       UIView.animate(withDuration: 0.2, animations: {
                           self?.tableView.alpha = 0.0
                       })
                   }else {
                       self?.activityIndicator.stopAnimating()
                       UIView.animate(withDuration: 0.2, animations: {
                           self?.tableView.alpha = 1.0
                       })
                   }
               }
           }

           viewModel.reloadTableViewClosure = { [weak self] () in
               DispatchQueue.main.async {
                   self?.tableView.reloadData()
               }
           }
           
           viewModel.initFetch()

       }
}

extension CategoryListViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? JokeDetailViewController,
            let category = viewModel.selectedCategory {
            vc.category = category
        }
    }
}

extension CategoryListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CategoryTableViewCell else {
            fatalError("Cell not exists in storyboard")
        }
        
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.categoryListCellViewModel = cellVM
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        self.viewModel.userPressed(at: indexPath)
        return indexPath
    }

}


