//
//  ChuckManager.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation

import Foundation
import Moya
import RxSwift


protocol ChuckManagerProtocol {
    func getCategories() -> [String]
}

public class ChuckManager: ChuckManagerProtocol {
 
    static let provider = MoyaProvider<ChuckService>()
    
    public static func getCategories() -> Observable<[String]> {
        return self.provider.rx.request(.categories)
        .asObservable()
        .filterSuccessfulStatusCodes()
        .map([String].self)
    }
    
    func getCategories() -> [String] {
           return []
    }
    
    public static func getJoke(category: String) -> Observable<Joke>{
        return self.provider.rx.request(.joke(category: category))
              .asObservable()
              .filterSuccessfulStatusCodes()
              .map(Joke.self)
      }
}
