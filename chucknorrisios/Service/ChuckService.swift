//
//  ChuckService.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation
import Moya
import RxSwift


enum ChuckService {
    case categories
    case joke(category: String)
}

// MARK: - TargetType Protocol Implementation
extension ChuckService: TargetType {
    var baseURL: URL { return URL(string: "https://api.chucknorris.io/jokes/")! }
    var path: String {
        switch self {
        case .categories:
            return "/categories"
        case .joke(let category):
            return "/random"
        }
    }
    var method: Moya.Method {
        switch self {
        case .categories, .joke:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .categories: // Send no parameters
            return .requestPlain
        case let .joke(category): // Always send parameters as JSON in request body
            return .requestParameters(parameters: ["category": category], encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .categories:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        case .joke(let category):
            return "{\"category\": \(category)".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
