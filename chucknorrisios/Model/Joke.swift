//
//  Joke.swift
//  chucknorrisios
//
//  Created by Gustavo Melki on 05/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation

public struct Joke: Codable {
    
    public let categories: [String]?
    public let created_at: String?
    public let icon_url: String?
    public let id: String?
    public let updated_at: String?
    public let url: String?
    public let value: String?
    
    enum CodingKeys: String, CodingKey {
        case categories
        case created_at
        case icon_url
        case id
        case updated_at
        case url
        case value
        
    }
    
    
}
