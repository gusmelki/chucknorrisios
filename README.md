## Desafio Chuck Norris

### Setup
Clone o projeto e execute pod install no diretório.


### Observações:
1. Arquitetura MVVM
2. Para a camada de serviço foi utilizado uma biblioteca MoyaRx. Baseado na descrição da vaga, acredito que foi uma oportunidade expor o conhecimento em programação reativa.
3. Para os recursos de serialização e deserealização, foi utilizado recurso nativos (Codable/Decodable).

---

