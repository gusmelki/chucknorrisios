//
//  CategoryListViewModelTests.swift
//  chucknorrisiosTests
//
//  Created by Gustavo Melki on 08/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import XCTest
import RxSwift
@testable import chucknorrisios

class CategoryListViewModelTests: XCTestCase {
    
    var sut: CategoryListViewModel!
    var mockAPIService: MockApiService!
    
    override func setUp() {
        super.setUp()
        mockAPIService = MockApiService()
        sut = CategoryListViewModel(apiService: mockAPIService)
    }
    
    override func tearDown() {
        sut = nil
        mockAPIService = nil
        super.tearDown()
    }
    
    func test_fetch_categories() {
        // Given
        var categories = mockAPIService.getCategories()
        categories = [String]()
        
        // When
        sut.initFetch()
        
        // Assert
        XCTAssert(mockAPIService!.isFetchPopularCateforyCalled)
    }
    
    func test_create_cell_view_model() {
        
           // Given
        let categories = StubGenerator().stubCategories()
           var cat = mockAPIService.getCategories()
           cat = categories
           let expect = XCTestExpectation(description: "reload closure triggered")
           sut.reloadTableViewClosure = { () in
               expect.fulfill()
           }
        let cellNumber = 16
           
           // When
           sut.initFetch()
           mockAPIService.fetchSuccess()
           
           
           // Number of cell view model is equal to the number of CATEGORIES
           XCTAssertEqual( cellNumber, cat.count )
           
           // XCTAssert reload closure triggered
           wait(for: [expect], timeout: 5.0)
           
    }
       
    
    func test_cell_view_model() {
        
        //Given category
        let categoryAnimal = "animal"
        
        // When creat cell view model
        let cellViewModel = sut!.createCellViewModel(category: categoryAnimal)

        
        // Assert the correctness of display information
        XCTAssertEqual(categoryAnimal, cellViewModel.category)
        
    }
}


//MARK: State control
extension CategoryListViewModelTests {
    private func goToFetchCategoryFinished() {
        
        var categories = mockAPIService.getCategories()
        categories = StubGenerator().stubCategories()
        sut.initFetch()
        mockAPIService.fetchSuccess()
    }
}

class MockApiService: ChuckManagerProtocol {
   

    var isFetchPopularCateforyCalled = false
    var completeClosure: [String] = []
 
    
    func getCategories() -> [String] {
        isFetchPopularCateforyCalled = true
        completeClosure = []
        return []
    }
    
    func fetchSuccess() {
        completeClosure = []
    }
    
}

class StubGenerator {
    func stubCategories() -> [String] {
        
        let categories = ["animal","career","celebrity","dev","explicit",
        "fashion","food","history","money","movie","music",
        "political","religion","science","sport","travel"]
        
        return categories
    }
}


