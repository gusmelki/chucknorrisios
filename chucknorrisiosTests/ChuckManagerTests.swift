//
//  ChuckManagerTests.swift
//  chucknorrisiosTests
//
//  Created by Gustavo Melki on 08/11/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import XCTest
import RxSwift
@testable import chucknorrisios

class ChuckManagerTests: XCTestCase {
    
    var sut: ChuckManager?
    let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_fetch_categories() {

        // When fetch categories
        let expect = XCTestExpectation(description: "callback")

        ChuckManager.getCategories().subscribe(onNext: { (result) in
            print(result)
            expect.fulfill()
            XCTAssertEqual( result.count, 16)
            for category in result {
                XCTAssertNotNil(category)
            }
        }, onError: { (error) in
        }, onCompleted: {
        }).disposed(by: disposeBag)

        wait(for: [expect], timeout: 3.1)
    }
    
}

